# synchron-fullstack

## Prerequisites

node, npm, and Python 3 are required. This code was tested using only the following versions:

* node v10.15.3
* npm v6.13.1
* Python 3.9.6

## Building and running

To run the backend:

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python setup.py
flask run
```

To run the frontend (use a separate terminal):

```
cd client
npm install
npm run serve
```

Then open a browser to `http://localhost:8080`.

![Example](/screenshot.png)

## Notes

1. I am using the `nltk` Python library for the word list. Before it can be used, the corpus must be downloaded to a local directory. This can be accomplished by running `setup.py` (or the following code:)

```
import nltk
nltk.download('words')
``` 

2. The conversion essentially works by considering a password as a base95 string (using the 95 printable ASCII characters to represent each digit), and considering a passphrase as a base N string (where N is the number of words in the dictionary). So the `fromBase` and `toBase` functions do the work of converting between those two formats, which are represented as strings by either `encode('utf-8')` (passwords) or a dictionary lookup (passphrase).

3. The `nltk` library has at least two useful word lists, `en` and `en-basic`. `en` has a much longer list of words compared to `en-basic` (235885 to 850), which yields shorter passphrases for the same length of password, but may be harder to use in practice.

4. Passwords are limited to _printable_ ASCII characters and leading/trailing whitespace is ignored. For input sanitation, passwords are limited to 1-64 characters in length.

5. [Vue](https://vuejs.org/) v2 is used for the frontend. A single component (`Translator.vue`) provides an input box, button, and output `span`, with the text and request URI set by the component props. [Bootstrap](https://getbootstrap.com/) is used for styling.

6. HTTPS is not used. In a real application, it would never be acceptable to transmit passwords in cleartext!