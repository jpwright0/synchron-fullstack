from flask import Flask, jsonify, request
from flask_cors import CORS
import urllib

from nltk.corpus import words

import math
import random

WORD_LIST = words.words('en') # or 'en-basic'
WORD_LIST_SIZE = len(WORD_LIST)

# shuffle the word list (in a repeatable way, by setting a particular seed)
random.seed(505075)
random.shuffle(WORD_LIST)

# generate dictionaries for linear time lookup
WORD_DICT = {k: v for v, k in enumerate(WORD_LIST)}
WORD_DICT_REV = {v: k for v, k in enumerate(WORD_LIST)}

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

# convert an integer (base 10) value `i` to a list of base `n` digits
def toBase(i,n):
    if i < n:
        return [i]
    else:
        return toBase(i//n,n) + [i%n]

# convert a list `d` of digits in base `n` to an integer (base 10) value
def fromBase(d,n):
    v = 0
    for i in range(0,len(d)):
        # no digit should be greater than its base
        if (d[i] >= n):
            raise ValueError('Invalid input for given base.')
        v += pow(n,len(d)-i-1) * d[i]
    return v

# passphrase to password
@app.route('/p2w', methods=['GET'])
def p2w():
    p = request.args.get('in')

    ps = p.split(' ')
    if len(ps) < 1:
        return "Passphrase must contain at least 1 word.", 400

    if not all(x in WORD_DICT for x in ps):
        return "Invalid passphrase.", 400

    pi = [WORD_DICT[x] for x in ps]

    # convert from base N to base 95
    pb = fromBase(pi, WORD_LIST_SIZE)
    pv = toBase(pb, 95)
    
    # add offset before decoding to get in printable character range
    pv = [x + 32 for x in pv]
    pv = bytes(pv)

    try:
        w = pv.decode('ascii')
    except:
        return "Invalid passphrase.", 400

    return jsonify(w)

# password to passphrase
@app.route('/w2p', methods=['GET'])
def w2p():
    w = request.args.get('in')

    if len(w) < 1 or len(w) > 64:
        return "Password length must be between 1 and 64 characters.", 400

    # encode string as ASCII
    try:
        we = w.encode('ascii')
    except:
        return "Password must contain only ASCII characters.", 400

    if True in (x < 32 for x in we):
        return "Password must contain only printable ASCII characters.", 400

    # convert from base 95 to base N
    # first subtract offset of printable character range
    we = [x - 32 for x in we]
    wi = fromBase(we, 95)
    wb = toBase(wi, WORD_LIST_SIZE)

    # get corresponding word for each part
    ws = [WORD_DICT_REV[x] for x in wb]

    # concat words into passphrase
    p = ' '.join(ws)

    return jsonify(p)

if __name__ == '__main__':
    app.run()